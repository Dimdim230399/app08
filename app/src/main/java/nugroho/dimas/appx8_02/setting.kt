package nugroho.dimas.appx8_02



import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.seting_activity.*


class setting : AppCompatActivity(), View.OnClickListener {

    lateinit var pref : SharedPreferences
    val arrayWarnaBackground = arrayOf("Blue", "Yellow", "Green", "Black")
    lateinit var adapterSpin: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.seting_activity)
        seekBar.setOnSeekBarChangeListener(onSeek)
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayWarnaBackground)
        spinner.adapter = adapterSpin

        button.setOnClickListener(this)


    }

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {
        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {
        }
    }

    override fun onClick(v: View?) {

        Toast.makeText(this,"Data di simpan" ,Toast.LENGTH_LONG).show()
        var aa  = Intent(this,MainActivity::class.java)
        startActivity(aa)

    }

}

