package nugroho.dimas.appx8_02

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var  Preference : SharedPreferences
    var PrefKey  = "setting"
    val font_size = "font_size_header"
    val default_font_header = 24
    val  teks  = "teks"
    val def_head = "Konser Avenged SevenFold"
    val def_title = "Avengge Seven Fold"
    val def_isi = ""
    val default_font_Title = 22
    val color ="color"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Preference = getSharedPreferences(PrefKey, Context.MODE_PRIVATE)
        header.setText(Preference.getString(teks,def_head))
        header.textSize = Preference.getInt(font_size,default_font_header).toFloat()
        judul.setText(Preference.getString(teks,def_title))
        judul.textSize = Preference.getInt(font_size,default_font_Title).toFloat()
        isi.setText(Preference.getString(teks,def_isi))

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInf = menuInflater
        mnuInf.inflate(R.menu.menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemsetting->{
                var aa = Intent(this,setting::class.java)
                startActivity(aa)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
